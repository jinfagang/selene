package main

import (
	"log"
	"gitlab.com/jinfagang/colorgo"
)

func init() {
	log.SetPrefix("[main center]")
	log.SetFlags(log.Ldate | log.Lshortfile)
}

type DBInfo struct {
	Driver string
	DB string
	Table string
	user string
	password string
}

func main() {
	cg.PrintlnBlue("Welcome to Selene - Extensible Scraper in Go.")
	cg.PrintlnGreen(` __    __  __    __    __  __
/ _\  /__\/ /   /__\/\ \ \/__\
\ \  /_\ / /   /_\ /  \/ /_\
_\ \//__/ /___//__/ /\  //__
\__/\__/\____/\__/\_\ \/\__/
                              `)
	cg.PrintlnRed("Author: Jin Tian.")


	//ScrapQiuShiBaiKe()

	// scrap weixin articles and save into database
	db_weixin := GetWeiXinDBInfo()
	ScrapWeiXin(&db_weixin)
}
