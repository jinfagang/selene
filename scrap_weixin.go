package main

import (
	"log"
	"github.com/PuerkitoBio/goquery"
	"gitlab.com/jinfagang/colorgo"
	"time"
	"strconv"
	"fmt"
	_"github.com/lib/pq"
	"database/sql"
)


/* this file scrap weixin article from 搜狗 */
/* category map:
   0: 首页
   1: 精选
   2: 热点 */


type WeiXinArticle struct {
	ArticleTitle   string
	ArticleUrl     string
	ArticleInfo    string
	ArticleSource  string
	ArticleTime    string
	ArticleContent string

	ArticleCategory int

	ThumbImageUrl string
}

func getThumbImageUrl(articleUrl *string) string{

	// get the article detail and thumb image
	docArticleDetail, err := goquery.NewDocument(*articleUrl)
	if err != nil{
		log.Print("some error in goquery: " + err.Error())
	}

	thumbImageUrl, _ := docArticleDetail.Find("#img-content").Find("img").Eq(2).Attr("data-src")
	return thumbImageUrl
}


func printArticle(article *WeiXinArticle){
	cg.PrintlnCyan("Title: ", (*article).ArticleTitle)
	cg.PrintlnCyan("Source: ", (*article).ArticleSource)
	cg.PrintlnCyan("Info: ", (*article).ArticleInfo)
	cg.PrintlnCyan("Url: ", (*article).ArticleUrl)
	cg.PrintlnCyan("Thumb: ", (*article).ThumbImageUrl)
}

func saveToPostGreSQL(db sql.DB, tableName string, article *WeiXinArticle){
	queryString := "INSERT INTO " + tableName +
		"(article_title,article_category,article_info,article_url,article_source,article_time,article_content,thumb_url,add_time)" +
		" VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9) RETURNING id"
	stmt, _ := db.Prepare(queryString)

	t := time.Now()
	addTime := t.Format("2006-01-02 15:04:05")

	// TODO: 为了防止重复添加数据，应该判断数据库中是否包含这条数据，如果有就pass掉
	res, err := stmt.Exec((*article).ArticleTitle, (*article).ArticleCategory, (*article).ArticleInfo, (*article).ArticleUrl,
		(*article).ArticleSource, (*article).ArticleTime, (*article).ArticleContent, (*article).ThumbImageUrl, addTime)
	printArticle(article)
	if err != nil {
		cg.PrintlnYellow("存储数据时出现了错误: ", err.Error())
	}else {
		fmt.Println(res)
		cg.PrintlnBlue("successfully insert a record.")
		cg.PrintlnBlue("---------------------------------------\n")
	}
}


func ScrapWeiXin(dbInfo *DBInfo){
	db, err := sql.Open((*dbInfo).Driver, "user=" + (*dbInfo).user + " password=" + (*dbInfo).password +
		" dbname=" + (*dbInfo).DB+ " sslmode=disable")
	if err != nil {
		cg.PrintRed("链接数据库时出现错误: ", err.Error())
	}
	cg.PrintlnYellow("==: database information: ", (*dbInfo).DB, " ", (*dbInfo).Table)

	for {
		doc, err := goquery.NewDocument("http://weixin.sogou.com")
		if err != nil{
			log.Print("some error in goquery: " + err.Error())
		}

		newestArticles := []WeiXinArticle{}

		log.Print("---- start scrap normal -----")
		// find the normal news
		doc.Find("ul.news-list li").Each(func(i int, s *goquery.Selection) {

			articleTitle := s.Find("div.txt-box").Find("h3").Text()
			articleUrl, _ := s.Find("div.txt-box").Find("h3").Find("a").Attr("href")
			articleInfo := s.Find("div.txt-box").Find("p").Text()
			articleSource := s.Find("div.txt-box").Find("div.s-p a").Text()
			timeStampString, _ := s.Find("div.txt-box").Find("div.s-p").Attr("t")
			timeStamp, _ := strconv.ParseInt(timeStampString, 10, 64)
			t := time.Unix(timeStamp, 0)
			articleTime := t.Format("2006-01-02 15:04:05")

			thumbImageUrl := getThumbImageUrl(&articleUrl)
			article := WeiXinArticle{
				ArticleTitle:    articleTitle,
				ArticleUrl:      articleUrl,
				ArticleInfo:     articleInfo,
				ArticleSource:   articleSource,
				ArticleTime:     articleTime,
				ArticleContent:  "",
				ArticleCategory: 0,
				ThumbImageUrl:   thumbImageUrl,
			}
			newestArticles = append(newestArticles, article)
		})

		// find the 编辑精选, 每天只有在8点更新
		nowHour := time.Now().Hour()
		if nowHour == 8 {
			log.Print("---- start scrap 精选 -----")
			doc.Find("ul.news-list-right li").Each(func(i int, s *goquery.Selection) {
				articleTitle := s.Find("div.txt-box").Find(".p1").Text()
				articleUrl, _ := s.Find("div.txt-box").Find(".p1").Find("a").Attr("href")
				articleInfo := ""
				articleSource := s.Find("div.txt-box").Find(".p2").Text()
				//articleTime := s.Find("div.txt-box").Find("p2").Find("span").Text()

				t := time.Now()
				articleTime := t.Format("2006-01-02 15:04:05")
				thumbImageUrl := getThumbImageUrl(&articleUrl)
				article := WeiXinArticle{
					ArticleTitle:    articleTitle,
					ArticleUrl:      articleUrl,
					ArticleInfo:     articleInfo,
					ArticleSource:   articleSource,
					ArticleTime:     articleTime,
					ArticleContent:  "",
					ArticleCategory: 1,
					ThumbImageUrl:   thumbImageUrl,
				}
				newestArticles = append(newestArticles, article)
			})
		}


		cg.PrintlnRed("scraped all ", len(newestArticles), " articles.")
		for i := 0; i < len(newestArticles); i++{
			saveToPostGreSQL(*db, (*dbInfo).Table, &newestArticles[i])
		}

		// 每个循环都记录一下开始日期，在爬取精选时，要对比开始日期和这个loop开始的日期，如果隔天就加入到数据库，如果不就pass
		// 每隔3个小时更新一次
		cg.PrintlnWhite("\n\n************************ rest for break ************************\n\n")
		time.Sleep(3*60 * time.Minute)
	}


}