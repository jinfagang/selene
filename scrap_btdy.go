package main

import (
	"fmt"
)

// This file will scrap all movie information on this site: http://www.btbtdy.com
// All the target will save into a table we call it video
// Video contains: Movie, TV, Animation, BlueRay, VR, 3D
// Very category contains many field: 动作, 科幻, 校园，家庭，儿童，喜剧，....

type Video struct {
	// video info
	VideoTitle string
	VideoStarring string
	VideoIntro string
	VideoReleaseDate string
	VideoPostUrl string
	VideoDirector string

	UpdateTime string
	Region string
	Language string

}


func ScrapBtdy() {
	fmt.Println("hello, Go lang.")
}
